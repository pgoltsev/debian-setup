#!/usr/bin/env bash
# Run script "wget --quiet -O - https://gitlab.com/pgoltsev/debian-setup/-/raw/master/install_docker.sh | sh".
set -e

if [ $UID != 0 ]; then
  echo "Root permissions required. Run the script as root user"
  exit 1
fi

apt-get update
apt-get upgrade -y

apt-get install \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     software-properties-common -y

curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -

add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"

apt-get update
apt-get install docker-ce -y
# Install docker-compose from official docs.
curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose