#!/usr/bin/env bash
# Run script "wget --quiet -O - https://gitlab.com/pgoltsev/debian-setup/-/raw/master/install_packages.sh | sh".
set -e

if [ $UID != 0 ]; then
  echo "Root permissions required. Run the script as root user"
  exit 1
fi

apt-get update
apt-get upgrade -y

apt-get install sudo git acl curl -y