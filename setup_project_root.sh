#!/usr/bin/env bash
# Run script "wget --quiet -O - https://gitlab.com/pgoltsev/debian-setup/-/raw/master/setup_project_root.sh | sh".
set -e

if [ $UID != 0 ]; then
  echo "Root permissions required. Run the script as root user"
  exit 1
fi

PROJECT_ROOT=/projects
mkdir "$PROJECT_ROOT"
chown root:users "$PROJECT_ROOT"
chmod 775 "$PROJECT_ROOT"
chmod g+s "$PROJECT_ROOT"
setfacl -d -m g::rwx "$PROJECT_ROOT"