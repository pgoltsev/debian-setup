#!/usr/bin/env bash
# Run script "wget --quiet -O - https://gitlab.com/pgoltsev/debian-setup/-/raw/master/setup_user.sh | sh".
set -e

if [ $UID != 0 ]; then
  echo "Root permissions required. Run the script as root user"
  exit 1
fi

USERNAME="$1"
useradd -m "$USERNAME"
usermod -aG sudo "$USERNAME"

cat <<EOT >> "/home/$USERNAME/.bashrc"
shopt -s histappend
shopt -s cdspell
shopt -s cmdhist
shopt -s autocd
shopt -s globstar
complete -cf sudo
PROMPT_DIRTRIM=2
HISTSIZE=4000
HISTFILESIZE=4000
HISTCONTROL=ignoredups
EOT